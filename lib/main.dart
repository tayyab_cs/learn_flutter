import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(),
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text('Backgroud Image'),
        ),
        body: MyHome(),
      ),
    );
  }
}

class MyHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      decoration: BoxDecoration(
        // color: const Color(0xff7c94b6),
        image: DecorationImage(
          image: AssetImage('images/castle.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      child: new Center(
        child: Text(
          'Welcome',
          style: TextStyle(
            fontSize: 42.0,
            fontWeight: FontWeight.bold,
            // color: Colors.white,
          ),
        ),
      ),
    );
  }
}
